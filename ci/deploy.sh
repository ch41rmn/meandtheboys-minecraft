#!/usr/bin/env bash

set -x

export COMPOSE_FILE=src/main/resources/meandtheboys/docker-compose.yaml

bash ci/undeploy.sh

docker-compose pull
docker-compose up -d
