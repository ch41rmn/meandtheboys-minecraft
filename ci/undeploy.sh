#!/usr/bin/env bash

set -x

export COMPOSE_FILE=src/main/resources/meandtheboys/docker-compose.yaml

docker-compose down
